# Dockerfile
# FROM directive instructing base image to build upon
FROM python:3-onbuild
MAINTAINER fiasco@lucacloud.info

# EXPOSE port 8000 to allow communication to/from server
EXPOSE 8000

# setup healthcheck for database connectivity
COPY mysqlping.py /mysqlping.py
RUN chmod +x /mysqlping.py

HEALTHCHECK --interval=60s --timeout=10s --start-period=120s CMD [ "/mysqlping.py" ]

# CMD specifcies the command to execute to start the server running.
VOLUME ["/usr/src/app/static"]
CMD ["/usr/src/app/start.sh"]
# done!
