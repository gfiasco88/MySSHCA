# MySSHCA - SSH Certified Authority with MySQL and DJANGO rest

__Author:__ Gian Luca Fiasco

__Mail:__ gfiasco88@gmail.com

![](http://loomchild.net/files/2015/09/docker-black.png)  ![](https://chocolatey.org/content/packageimages/python.3.6.3.svg) ![](http://erlycoder.com/themes/site4fast/images/mysql.png)

## Problem Domain
The password has failed. That ubiquitous annoyance of the digital age - the computer password - has proved itself to be profoundly unsafe. People forget them, or worse, use the same one over and over, for everything from buying a book on Amazon.com to protecting a bank account. Hackers half a planet away steal them by the thousands, if not millions.

It’s not feasible to type a password over and over, as engineer are required to quickly jump from server to server to troubleshoot and fix multiple issues which can occur from multiple servers.

In addition IT engineers tend to automate tasks building their tools, which can spread from a little bash script to a complex platform, and this led to the problem of how to authenticate while running the script.

__What this can led to__
* storing the password in plain text inside the script
* storing the password inside the OS keychain

While the first is clearly dangerous, the second one instead is more secure in a non-shared machine however  
the purpose of enhanced gets defeated when script are running in a shared machine where multiple people have root or unrestricted sudo access:  

```
user2@example$: sudo su user1
Enter sudo password for user2:
user1@example$: python
>>import keyring
>> keyring.get_password(‘service’,’account’)
foobar
```

Many companies out there have implemented password less authentication, using SSO, Kerberos, special 2 factors fobs and SSH keys

## Passwordless SSH - using SSH keys 
In Public Key for authentication, the sender (supplicant) is attempting to prove that he knows the true party’s secret private key. The sender encrypts a message with his own private key. the receiver (verifier) then decrypts the message with the true party’s public key. 

**Two keys are required**
1. Supplicant Private RSA Key
2. Supplicant Public RSA Key

The Private Key is owned and used by the supplicant (the client) and it is never shown to anyone or store outside the client.
The public Key instead has to be known to the Authenticator (the SSH server) in order to authenticate the supplicant

# SSH CA Authority

how and where we store the SSH keys on linux Systems:

Three common ways:

1. On each SSH servers inside the user $HOME/.ssh/authorized_keys
   * This can be distributed and enforced using puppet accross the farm 
2. On a LDAP server
3. On roaming NFS user share

## A 4th WAY: MySQLCA

This is a completed and indipendent method to authenticate your users which can be kept aside to LDAP/Kerberos and other authentication methods, as includes a new player "MYSQL-CA" Server.

Basically you need to store all the public rsa keys inside a mysql database, and each SSH server will then query the MySQL CA to authenticate the user, as the picture below shows

![image](http://gitlab.lucacloud.info/fiasco/MySSHCA/uploads/5d2c075a8083744bb49237c3f5d75195/diagram.jpg)

### How it Work

This project is composed in three parts:

1. **Server FrontEnd**
2. **Server Backend** 
3. **Client Setup/Script**

The frontend uses **rest** framework (django) to update/retrive user public-keys 
The backend uses **mysql** to store and serialize user keys together with user permission in modify them

The client setup is composed by:
- [pam_ssh_agent_auth] (https://www.unix.com/man-page/centos/8/pam_ssh_agent_auth/)  
- [simple python script](https://gitlab.lucacloud.info/fiasco/MySSHCA/blob/master/client/check-mysql-key.py)

#### Workflow


## SERVER SETUP

the following instructions are to be executed on the host that will act as CA.

it is meant to be used with nginx as a SSL proxy in front and mysql, possibly on the same host 


### Docker image

***dockrepo.lucacloud.info/fiasco/mysshca***


### Variabes
 * DB_USER 
 * DB_PASS 
 * DB_HOST
 * DB_PORT

```bash
docker run -p8000:8000 -e DB_HOST=localhost -e DB_PORT=3400 -e DB_PASS=foobar  -d -it dockrepo.lucacloud.info/fiasco/mysshca
```

see docker-compose.yml for an example deployment.

## Installation from Source

Change mysshca/setting.py according to your mysql setup

```bash
# INSTALL MySQL/MariaDB
sudo apt-get install mariadb-server
echo 'CREATE DATABASE mysshca'| mysql -u root
echo "GRANT ALL ON mysshca.* TO 'django'@'localhost' IDENTIFIED BY 'Fmf!^Vc*PJNbzc8N'" | mysql -u root
git clone https://gitlab.lucacloud.info/fiasco/MySSHCA.git
cd MySSHCA; pip install -r requirements.txt
./start.sh
```


# CLIENT SETUP
**Setup sshd_conf to authenticate SSHKEYS against mysql**
1. sudo vi /etc/ssh/sshd_confid
2. add the following lines

```
	AuthorizedKeysCommand ${PATH_TO_FOLDER}/MySSHCA/check-mysql-key
 	AuthorizedKeysCommandUser root     # or system user that can run the script
 	#AuthorizedKeysCommandRunAs root // if you are on centos 
```

**Setup sudo to use check-mysql-key**
1. Retrive the Binaries of pam-ssh-agent-auth-0.10.2 or higher
   * this git repo has a working version that I have fixed for the redhat
2. install the libpam-ssh-agent-auth/pam-ssh-agent-auth-0.10.2

```bash
# DEBIAN 8/9 (not sure for wheezy)
sudo apt-get install libpam-ssh-agent-auth
```

Now you are almost set, al you need is to configure the module, two files needs to be edited:

__/etc/sudoers (if sudo < 1.8.5*)__

<pre>
#
# This file MUST be edited with the 'visudo' command as root.
#
# Please consider adding local content in /etc/sudoers.d/ instead of
# directly modifying this file.
#
# See the man page for details on how to write a sudoers file.
#
Defaults    env_reset
Defaults    mail_badpass
Defaults    secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
<b>Defaults     env_keep += "SSH_AUTH_SOCK"</b> #add this line

# Host alias specification

# User alias specification

# Cmnd alias specification

# User privilege specification
root    ALL=(ALL:ALL) ALL
# Allow members of group sudo to execute any command
%sudo    ALL=(ALL:ALL) ALL

# See sudoers(5) for more information on "#include" directives:

#includedir /etc/sudoers.d
</pre>

__/etc/pam.d/sudo__
add this line:

```
auth       sufficient pam_ssh_agent_auth.so authorized_keys_command=${PATH_TO_FOLDER}/MySSHCA/check-mysql-key  authorized_keys_command_user=root
```

# Django Impersonate

to impersonate another user and manage its keys:
1. Login with an admin user /admin
2. visit /impersonate/list
