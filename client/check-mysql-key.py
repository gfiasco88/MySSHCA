#!/usr/bin/env python

######################################################
# Script to retrieve SSH pub keys                    #
#                                                    #
# Take configuration from config file:               #
#  - CA server FQDN                                  #
#  - Allowed Domain                                  #
#                                                    #
# Input: sys.argv[1] only                            #   
######################################################

import requests
import sys
from config import *

if __name__=='__main__':
    
    head = {"Content-type": "application/json"}
    URI="https://{}/pubkeys/?owner={}&domain={}"
    user=sys.argv[1]
    domain=config["domain"]
    certpath=config["cacert"]
    authenticator=config["caserver"]
    r=requests.get(URI.format(authenticator,user,domain),headers=head,verify=certpath)
    
    result=''
    for ssh in r.json():
        result+=ssh['ssh_key']+'\n'
    
    print result