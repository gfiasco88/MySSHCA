#!/usr/bin/env python
import os
#from manage import *
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysshca.settings")
import django
django.setup()

from django.contrib.auth.models import User


if __name__=='__main__':

    boolAdmin=len(User.objects.filter(is_superuser=1))==0
    if boolAdmin:
        u = User(username='admin')
        u.set_password('admin')
        u.is_superuser = True
        u.is_staff = True
        u.force_password_change = True
        u.save()

