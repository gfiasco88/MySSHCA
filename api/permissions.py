from rest_framework.permissions import BasePermission
from .models import Pubkeys

class IsOwner(BasePermission):
    """Custom permission class to allow only record owners to edit them."""

    def has_object_permission(self, request, view, obj):
        """Return True if permission is granted to the record owner."""
        if isinstance(obj, Pubkeys):
            return obj.owner == request.user
        return obj.owner == request.user
