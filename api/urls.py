from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import PrimaryView,GeneralPubkeysView, DetailsView
from rest_framework.routers import DefaultRouter


#router=DefaultRouter()

#router.register(r'pubkeys', PrimaryView, base_name='keys')
#urlpatterns = router.urls


urlpatterns = {
     url(r'^auth/', include('rest_framework.urls', # ADD THIS URL
                               namespace='rest_framework')), 
    url(r'^pubkeys/$', GeneralPubkeysView.as_view(), name="keys"),
    url(r'^pubkeys/(?P<pk>[0-9]+)/$', DetailsView.as_view(), name="details")
#    url(r'^pubkeys/[0-9]+$', IDview.as_view(), name="ID_redirect")
 
}

urlpatterns = format_suffix_patterns(urlpatterns)
