from rest_framework import serializers
from .models import  Pubkeys



class PubkeysSerializer(serializers.ModelSerializer):
   owner = serializers.ReadOnlyField(source='owner.username')
   class Meta:
       """Meta class to map serializer's fields with the model fields."""
       model = Pubkeys
       fields = ('id','owner','ssh_key','domain', 'date_created', 'date_modified')
       read_only_fields = ('date_created', 'date_modified')
