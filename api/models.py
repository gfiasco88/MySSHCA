# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Pubkeys(models.Model):
    #username = models.CharField(max_length=255, blank=False, unique=False)
    owner = models.ForeignKey('auth.User',
    related_name='pubkeys',
    on_delete=models.CASCADE)
    domain = models.CharField(max_length=255, blank=True, unique=False)
    ssh_key = models.TextField(unique=False,blank=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format(self.owner)
