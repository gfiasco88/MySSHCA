from rest_framework import generics, filters, views, viewsets, permissions
from django_filters.rest_framework import DjangoFilterBackend
from .serializers import PubkeysSerializer 
from .models import Pubkeys
from .permissions import IsOwner
from django.db import models
from django.contrib.auth.models import User

"""
   Features: 
      - owners can modify other can see
      - filtering based on owner, rsa_key and dates
      - partial updates and full updates of existing keys
"""


class GeneralPubkeysView(generics.ListCreateAPIView):
   """
   Allow combined filters such pubkeys/?owner=fiasco&domain=lucacloud.com
   """

   queryset = Pubkeys.objects.all()
   serializer_class = PubkeysSerializer
   filter_backends = (DjangoFilterBackend,)
   filter_fields = ('owner','ssh_key','domain', 'date_created', 'date_modified')
   permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsOwner)
   
   def perform_create(self, serializer):
      """Save the post data when creating a new bucketlist."""
      serializer.save(owner=self.request.user)

   def get(self, request, *args, **kwargs):
      try:
         param=request.query_params['owner']
         self.request.GET._mutable=True
         if not param.isdigit():
            """resolving username in PK of the foreign table """
            request.query_params['owner']=User.objects.get(username=param).pk
      except:
         pass
      return self.list(request,*args,**kwargs)


   def put(self, request, *args, **kwargs):
      result=self.list(request,*args,**kwargs)
      list_records=[ element['id'] for element in result.data]
      return result

class DetailsView(generics.RetrieveUpdateDestroyAPIView):
   """This class handles the http GET, PUT and DELETE requests."""
   queryset = Pubkeys.objects.all()
   serializer_class = PubkeysSerializer
   permission_classes = [permissions.IsAuthenticatedOrReadOnly]#,IsOwner]

   def put(self, request, *args, **kwargs):
        self.permission_classes.append(IsOwner)
        return self.partial_update(request, *args, **kwargs)

class PrimaryView(viewsets.ModelViewSet):

   serializer_class = PubkeysSerializer
   queryset = Pubkeys.objects.all()
   filter_backends = (DjangoFilterBackend,)
   filter_fields = ('owner','ssh_key','domain')

   #def update(self, request, *args, **kwargs):
   #   document = self.get_object()
   #   collection_pk = document.id
   #   request.data['id'] = collection_pk
   #   return super().update(request, *args, **kwargs)

   
   def put(self, request, *args, **kwargs):
      result=self.list(request,*args,**kwargs)
      list_records=[ element['id'] for element in result.data]
      print (list_records)
      self.update(request, *args, **kwargs)
     
     # return result
