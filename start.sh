#!/bin/bash
# Start Gunicorn processes

# Check database connectivity
until python /mysqlping.py 2> /dev/null; do 
    echo "DATABASE is not available - retry"
    sleep 3
done

echo "Collect static files"
python manage.py collectstatic --noinput
# Apply database migrations
echo "Apply database migrations"
python manage.py makemigrations
python manage.py migrate
python reset_admin_pw.py 

echo Starting Gunicorn.
NUM=$(expr `lscpu | grep '^CPU(s):'|awk '{print $2}'` + 1)
exec gunicorn mysshca.wsgi \
    --bind 0.0.0.0:8000 \
    --workers $NUM
